import React from "react";
import { Button } from "../Button/Button";
import cl from '../Heroimage/Heroimage.module.css'
import { Navbar } from "../Navbar/Navbar";


function Heroimage() {
    return (
        <div className={cl.heroimage}>
            <div className="bg">
                <Navbar classes={cl.navbar} />
                <div className={cl.desc_wrap}>
                    <span className={cl.desc_title}>
                        CloudBudget
                    </span>
                    <div className={cl.desc}>
                    <p className={cl.desc_main}>
                        Управление бюджетом в для всех.
                        Всего 69,95 € в месяц после 7-дневной
                        пробной версии за 4,99 €
                    </p>
                    </div>
                    <Button title="Попобовать"  cb={() => console.log('click btn ПОПРОБОВАТЬ')} styless={cl.btn}/>
                </div>
            </div>
        </div>
    )
}
export default Heroimage