

export const Button = ({title, cb, styless}) => {
    return (
        <button onClick={cb} className={styless}>
            {title}
        </button>
    )
}