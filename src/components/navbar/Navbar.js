import React from "react";

import {ReactComponent  as Logo} from "../../images/logo.svg"
import { Button } from "../Button/Button";

import cl from './Navbar.module.css'

export const Navbar = ({ classes }) => {

    const navBarTitleArr = ["Обзор", "Особенности", "Технология", "Контакты", "Войти"]
    return (
        <div className={`${classes}`}>
            <div className={cl.logo_block}>
                <Logo />
                <span className={cl.title}>CloudBudget</span>
            </div>

            <div className={cl.navbar_menu_block}>
            {navBarTitleArr.map(item => <span className={cl.navbar_title}>{item}</span>)}
            </div>

            <div>
                <Button title="Вход" cb={() => console.log('click')} styless={cl.btn}/>
            </div>
        </div>
    )
}
